const jwt = require('jsonwebtoken');
const config = require('../database/config.js');
const db = require('../database/db.js');
const Casos = db.casos;


//====================================
// Verificando email existent
//====================================
exports.revisarCasoExiste = (req, res, next) => {
    // -> Check Email is already in use
    Casos.findOne({
        where: {
            caso_numcaso: req.body.caso_numcaso
        }
    }).then(user => {
        if (user) {
            res.status(400).send("Fail -> el numero de caso ya esta en uso");
            return;
        }

        next();
    });
}