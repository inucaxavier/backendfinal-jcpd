const jwt = require('jsonwebtoken');
const config = require('../database/config.js');
const db = require('../database/db.js');
const User = db.user;


//====================================
//Verificar token
//====================================


exports.verificaToken = function (req, res, next) {

    var token = req.query.token;

    jwt.verify(token, config.secret, (err, decoded) => {
        if (err) {
            return res.status(401).json({
                ok: false,
                message: 'token incorrecto ',
                errors: err
            });
        }
        req.usuario = decoded.usuario
        next();

    })
}

//====================================
// Verificando email existent
//====================================
exports.revisarEmailexiste = (req, res, next) => {
    // -> Check Email is already in use
    User.findOne({
        where: {
            usu_email: req.body.usu_email
        }
    }).then(user => {
        if (user) {
            res.status(400).send("Fail -> Email ya esta en uso");
            return;
        }

        next();
    });
}


//====================================
// Controlando desde el backend modificaciones de ADMINISTRADOR
//====================================

exports.verificaAdminRol_o_Id = function (req, res, next) {

    var usuario = req.usuario;
    var id = req.params.id;
    if (usuario.rol === 'ADMIN_ROLE') {
        console.log()
        next()
        return
    } else {

        return res.status(401).json({
            ok: false,
            message: 'token incorrecto - no es admin',
            errors: {
                message: 'no es administrador'
            }
        });

    }
}