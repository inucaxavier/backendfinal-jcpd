// Ruta del middleware para autenticacion
var mdAutenticacion = require('../middlewares/autenticacion');
var casosMdd = require('../middlewares/casosMdd');
let upload = require('../database/multer.config.js');
const fileUpload = require('express-fileupload');



// RUTAS DE LOS CONTROLLERS
const controllerUsu = require('../controller/controllerUsuario');
const casos = require('../controller/casos.controller.js');
const datosafectado = require('../controller/datosafectado.controller.js');
const datosdenunciado = require('../controller/datosdenunciado.controller.js');
const datosdenunciante = require('../controller/datosdenunciante.controller.js');

const parroquias = require('../controller/parroquias.controller.js');
const provincias = require('../controller/provincias.controller.js');
const cantones = require('../controller/cantones.controller.js');
const sectores = require('../controller/sectores.controller.js');
const tparroquia = require('../controller/tipoparroquias.controller.js');

const analisisCasos = require('../controller/analisis_casos.controller.js');
const audiencia = require('../controller/audiencia.controller.js');
const audienciaprueba = require('../controller/audienciaprueba.controller.js');
const avocatoria = require('../controller/avocatoria.controller.js');
const derechoamenazado = require('../controller/derecho.amenazado.controller.js');
const discapacidad = require('../controller/discapacidad.controller.js');
const genero = require('../controller/genero.controller.js');
const identidad = require('../controller/identidad.controller.js');
const medidasemergentes = require('../controller/medidasemergentes.controller.js');
const artmedpro = require('../controller/articulomedidasproteccion.controller.js');
const proco = require('../controller/proco.controller.js');
const resolucion = require('../controller/resolucion.controller.js');
const revisionAudienciaPrueba = require('../controller/revicionaudienciaprueba.controller.js');
const tipocasos = require('../controller/tipo_casos.controller.js');
const tdenun = require('../controller/tipodenuncia.controller.js');
const tpersona = require('../controller/tipopersona.controller.js');
const tipoderechos = require('../controller/tipoderechos.controller.js');
const tiposmedidas = require('../controller/tipomedidas.controller.js');
const tderamenazas = require('../controller/tipoderechoamenazas.controller.js');
const fileWorker = require('../controller/file.controller.js');



module.exports = function (app) {


    app.use(function (req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        res.header("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, OPTIONS");
        next();
    });

    //====================================
    // CARGAR Y DESCARGAR ARCHIVOS
    //====================================
    app.post('/api/file/upload', upload.single("file"), fileWorker.uploadFile);

    app.get('/api/file/all', fileWorker.listAllFiles);

    app.get('/api/file/:id', fileWorker.downloadFile);

    /*     app.post('/api/subir/:caso_id', upload.single("file"), fileWorker.subirArchivo);
     */
    // -----------------------------------CRUD LOGIN LOGOUT---------------------------------------//


    app.get('/usuarios', controllerUsu.usuarios);
    app.get('/renuevaToken', mdAutenticacion.verificaToken, controllerUsu.expireUsuario);
    app.post('/usuario', mdAutenticacion.revisarEmailexiste, controllerUsu.crearUsuario, );
    app.put('/usuario/:id', [mdAutenticacion.verificaToken], controllerUsu.update);
    app.delete('/usuario/:id', mdAutenticacion.verificaToken, controllerUsu.delete);

    app.post('/login', controllerUsu.login);

    // Busqueda

    // CRUD CASOS

    app.post('/api/casos', casosMdd.revisarCasoExiste, casos.create);
    app.get('/api/casos', casos.findAll);
    app.get('/api/historiaCasos', casos.encontrarHistoricos);
    app.get('/api/caso', casos.findAllUltimo);
    app.get('/api/datosafectadoidcas/:caso_numcaso', casos.buscarAfectadoPorIdRelacional);


    app.get('/api/casos/:caso_numcaso', casos.findByCaso);
    app.put('/api/casos/:casosId', casos.update);
    app.delete('/api/casos/:casosId', casos.delete);
    app.get('/api/ca/:caso_id', casos.getPostById);
    app.get('/api/ca/:caso_numcaso', casos.findByCaso);

    app.get('/api/usu/:id', casos.findByUsuario);




    app.get("/dashboard/overview", casos.getDashboarOverviewCasos);
    app.post("/dashboard/casos/publish", casos.updatePublishState);
    app.get("/dashboard/caso/:key", casos.editDashboardCasobyKey);


    // DATOS AFECTADO
    app.post('/api/datosafectado', datosafectado.create);
    app.get('/api/datosafectado', datosafectado.findAll);
    app.get('/api/datosafectado/:datosafectadoId', datosafectado.findById);
    app.put('/api/datosafectado/:datosafectadoId', datosafectado.update);
    app.delete('/api/datosafectado/:datosafectadoId', datosafectado.delete);

    app.post('/api/datosafectado/:caso_numcaso', datosafectado.createByIdRelacional);
    app.get('/api/buscarRecursosCaso/:caso_numcaso', [casos.buscarAfectadoPorIdRelacional]);
    app.get('/api/datosafectadoidcaso/:caso_numcaso', casos.buscarAfectadoPorIdRelacional);

    app.post('/api/casos/:caso_id', datosafectado.crearAfectado);
    app.get('/todo/:busqueda', casos.busqueda);
    app.get('/coleccion/:tabla/:busqueda', casos.busquedaTablas);

    // DATOS DENUNCIADO
    app.post('/api/datosdenunciado', datosdenunciado.create);
    app.get('/api/datosdenunciado', datosdenunciado.findAll);
    app.get('/api/datosdenunciado/:datosdenunciadoId', datosdenunciado.findById);
    app.put('/api/datosdenunciado/:datosdenunciadoId', datosdenunciado.update);
    app.delete('/api/datosdenunciado/:datosdenunciadoId', datosdenunciado.delete);
    // DATOS DENUNCIANTE

    app.post('/api/datosdenunciante', datosdenunciante.create);
    app.get('/api/datosdenunciante', datosdenunciante.findAll);
    app.get('/api/datosdenunciante/:datosdenuncianteId', datosdenunciante.findById);
    app.put('/api/datosdenunciante/:datosdenuncianteId', datosdenunciante.update);
    app.delete('/api/datosdenunciante/:datosdenuncianteId', datosdenunciante.delete);
    app.get('/api/parrosum', datosdenunciante.incrementando);



    // CRUD ANALISIS CASOS
    app.post('/api/analisisCasos', analisisCasos.create);
    app.get('/api/analisisCasos', analisisCasos.findAll);

    //CRUD AUDIENCIA
    app.post('/api/audiencia', audiencia.create);
    app.get('/api/audiencia', audiencia.findAll);

    //CRUD AUDIENCIA PRUEBA
    app.post('/api/audienciaprueba', audienciaprueba.create);
    app.get('/api/audienciaprueba', audienciaprueba.findAll);
    app.get('/api/audienciaprueba/:audienciapruebaId', audienciaprueba.findById);

    //CRUD AVOCATORIA
    app.post('/api/avocatoria', avocatoria.create);
    app.get('/api/avocatoria', avocatoria.findAll);
    app.get('/api/avocatoria/:avocatoriaId', avocatoria.findById);
    app.put('/api/avocatoria/:avocatoriaId', avocatoria.update);
    app.delete('/api/avocatoria/:avocatoriaId', avocatoria.delete);

    //CRUD CANTONES
    app.get('/api/cantones', cantones.findAll);
    app.get('/api/cantones/:canId', cantones.findById);


    // DERECHOS AMANAZADO VIOLENTADO
    app.get('/api/amenazadoViolentado', derechoamenazado.findAll);
    app.get('/api/amenazadoViolentado/:davId', derechoamenazado.findById);

    // DISCAPACIDAD
    app.get('/api/discapacidad', discapacidad.findAll);
    app.get('/api/discapacidad/:disId', discapacidad.findById);

    // GENERO
    app.get('/api/genero', genero.findAll);
    app.get('/api/genero/:geneId', genero.findById);

    // IDENTIDAD
    app.get('/api/identidad', identidad.findAll);
    app.get('/api/identidad/:idenId', identidad.findById);

    // MEDIDAS DE EMERGENCIA
    app.post('/api/medidasemergentes', medidasemergentes.create);
    app.get('/api/medidasemergentes', medidasemergentes.findAll);
    app.get('/api/medidasemergentes/:medidasemergentesId', medidasemergentes.findById);
    app.put('/api/medidasemergentes/:medidasemergentesId', medidasemergentes.update);
    app.delete('/api/medidasemergentes/:medidasemergentesId', medidasemergentes.delete);

    // ARTICULO DE MEDIDAS DE PROTECCION 
    app.get('/api/artmedpro', artmedpro.findAll);
    app.get('/api/artmedpro/:mpaId', artmedpro.findById);

    //CRUD PARROQUIAS
    app.get('/api/parroquias', parroquias.findAll);
    app.get('/api/parroquias/:parroId', parroquias.findById);

    // PROCESAMIENTO DE CONOCIMIENTO
    app.get('/api/proco', proco.findAll);
    app.get('/api/proco/:procoId', proco.findById);

    //CRUD PROVINCIAS
    app.get('/api/provincias', provincias.findAll);
    app.get('/api/provincia/:provId', provincias.findById)

    // RESOLUCION 
    app.post('/api/resolucion', resolucion.create);
    app.get('/api/resolucion', resolucion.findAll);
    app.get('/api/resolucion/:resId', resolucion.findById);
    app.put('/api/resolucion/:resId', resolucion.update);
    app.delete('/api/resolucion/:resId', resolucion.delete);

    // REVICIÓN AUDIENCIA PRUEBA 
    app.post('/api/revisionAudienciaPrueba', revisionAudienciaPrueba.create);
    app.get('/api/revisionAudienciaPrueba', revisionAudienciaPrueba.findAll);
    app.get('/api/revisionAudienciaPrueba/:revaudipId', revisionAudienciaPrueba.findById);
    app.put('/api/revisionAudienciaPrueba/:revaudipId', revisionAudienciaPrueba.update);
    app.delete('/api/revisionAudienciaPrueba/:revaudipId', revisionAudienciaPrueba.delete);

    // SECTORES
    app.post('/api/sectores', sectores.create);
    app.get('/api/sectores', sectores.findAll);
    app.get('/api/sectores/:secId', sectores.findById);
    app.put('/api/sectores/:secId', sectores.update);
    app.delete('/api/sectores/:secId', sectores.delete);

    // TIPO CASOS 
    app.get('/api/tipocasos', tipocasos.findAll);
    app.get('/api/tipocasos/:tipocasosId', tipocasos.findById);

    // TIPO DENUNCIA
    app.get('/api/tipoDenuncia', tdenun.findAll);
    app.get('/api/tipoDenuncia/:tdenunId', tdenun.findById);

    // TIPO PARROQUIA
    app.get('/api/tparroquia', tparroquia.findAll);
    app.get('/api/tparroquia/:tparroId', tparroquia.findById);

    // TIPO PERSONA
    app.get('/api/tpersona', tpersona.findAll);
    app.get('/api/tpersona/:tpersonaId', tpersona.findById);

    // TIPO DERECHOS 
    app.get('/api/tder', tipoderechos.findAll);
    app.get('/api/tder/:tderId', tipoderechos.findById);

    // TIPO DE MEDIDAS
    app.get('/api/tiposmedidas', tiposmedidas.findAll);
    app.get('/api/tiposmedidas/:tmedidasId', tiposmedidas.findById);

    // TIPO DERECHO AMENAZAS 
    app.get('/api/tipoDerechoAmenazas', tderamenazas.findAll);
    app.get('/api/tipoDerechoAmenazas/:tderameId', tderamenazas.findById);

};