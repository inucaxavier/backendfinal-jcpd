const db = require('../database/db.js');
const Parroquias = db.parroquias;

exports.findAll = (req, res) => {
    Parroquias.findAll({
        where: {
            can_id: 102
        }
    }).then(parroquias => {
        res.send(parroquias);
    });
};
exports.findById = (req, res) => {
    const parr_id = req.params.parroId;
    Parroquias.findById(parr_id).then(parroquias => {
        res.send(parroquias);
    });
};