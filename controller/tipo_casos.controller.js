const db = require('../database/db.js');
const TipoCasos = db.tipocasos;

exports.findAll = (req, res) => {
    TipoCasos.findAll().then(tipocasos => {
        // Send all customers to Client
        res.send(tipocasos);
    });
};

// Find a Customer by Id
exports.findById = (req, res) => {
    const tica_id = req.params.tipocasosId;
    TipoCasos.findById(tica_id).then(tipocasos => {
        res.send(tipocasos);
    })
};