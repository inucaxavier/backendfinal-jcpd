const db = require('../database/db.js');
const config = require('../database/config.js').secret;
const User = db.user;
const sequelize = db.sequelize;


var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');

//====================================
// Verificacion de expiresIn token
//====================================

exports.expireUsuario = async (req, res) => {
    var token = jwt.sign({
        usuario: req.usuario
    }, config, {
        expiresIn: 7200
    });
    res.status(200).json({
        ok: true,
        token: token,
        toke: token.expiresIn

    })

}

//====================================
// Obtener todos los usuarios
//====================================
exports.usuarios = async (req, res, next) => {

    await User.findAll({
        order: sequelize.literal("id DESC"),
        attributes: ['id', 'usu_nombres', 'usu_apellidos', 'usu_cedula', 'usu_email', 'usu_rol']
    }).then(usuarios => {
        // Send all customers to Client
        res.status(200).json({
            ok: true,
            usuarios
        })
    }).catch(error => console.log("no existen casos" + error))

}

//====================================
// Crear un nuevo usuario
//====================================


exports.crearUsuario = (req, res, next) => {
    // Save to MySQL database
    User.create({
        usu_nombres: req.body.usu_nombres,
        usu_apellidos: req.body.usu_apellidos,
        usu_cedula: req.body.usu_cedula,
        usu_email: req.body.usu_email,
        usu_password: bcrypt.hashSync(req.body.usu_password, 10),
        usu_rol: req.body.usu_rol,

    }).then(usuario => {
        // Send created customer to client
        usuario.usu_password = ':)';
        res.status(200).json({
            ok: true,
            usuario: usuario,
            usuariotoken: req.usuario
        })
    });
};

exports.actualizarUsuario = (req, res, next) => {
    // Actualizar usuario to MySQL database
    var id = req.params.id;
    var body = req.body;
    User.findById(id, (err, usuario) => {

        if (err) {
            return res.status(500).json({
                ok: false,
                message: 'error al buscar usuario',
                errors: err
            })
        }
        if (!usuario) {
            return res.status(500).json({
                ok: false,
                message: 'el usuario con el id ' + id + 'no existe',
                errors: {
                    message: 'No existe un usuario con ese ID'
                }
            })
        }

        user.usu_nombres = body.usu_nombres;
        user.usu_apellidos = body.usu_apellidos;
        user.usu_cedula = body.usu_cedula;
        user.usu_email = body.usu_email;
        user.usu_rol = body.usu_rol;

        User.update((err, usuarioGuardado) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    message: 'Error al actualizar el usuario',
                    errors: err
                })
            }

            res.status(200).json({
                ok: true,
                usuario: usuarioGuardado
            })
        }).then(user => {
            // Send created customer to client
            res.status(200).json({
                ok: true,
                user: usuarioGuardado
            })
        });;

    });
    // Send created customer to client
};


//====================================
//Actulizando un usuario por Id
//====================================
exports.update = (req, res) => {
    const id = req.params.id;
    User.update({
            usu_nombres: req.body.usu_nombres,
            usu_apellidos: req.body.usu_apellidos,
            usu_cedula: req.body.usu_cedula,
            usu_email: req.body.usu_email,
            usu_rol: req.body.usu_rol
        },

        {
            where: {
                id: req.params.id
            }
        }).then(usuario => {
        res.status(200).json({
            ok: true,
            message: "updated successfully a caso with caso_id = " + id,
            usuario: req.usuario
        })
    });
};


//====================================
// Eliminar un usuario por el id
//====================================

exports.delete = (req, res) => {
    const id = req.params.id;
    User.findOne({
        where: {
            id: id
        }
    }).then(function (usuario) {
        if (usuario != null) {
            usuario.destroy().then(
                res.status(200).json({
                    ok: true,
                    message: 'deleted successfully a caso with id = ' + id
                }));
        } else {
            res.status(400).send({
                ok: false,
                message: "Caso no puede ser borrado por que no existe"
            });
        }
    });
};


//====================================
//LOGIN
//====================================


exports.login = (req, res) => {
    User.findOne({
        where: {
            usu_email: req.body.usu_email
        }
    }).then(usuario => {
        if (!usuario) {
            return res.status(404).send({
                reason: 'Usuario no encontrado!'
            });
        }
        // deciframos la contraseña y verificamos
        var passwordIsValid = bcrypt.compareSync(req.body.usu_password, usuario.usu_password);
        if (!passwordIsValid) {
            return res.status(401).send({
                auth: false,
                accessToken: null,
                reason: 'Contraseña Invalida!'
            });
        }

        // creamos un token
        usuario.usu_password = ':)';
        var token = jwt.sign({
            usuario: usuario
        }, config, {
            expiresIn: 60
        });

        res.status(200).send({
            ok: true,
            usuario,
            token: token,
            id: usuario.id,
            menu: obtenerMenu(usuario.usu_rol)
        });
    })
}

function obtenerMenu(ROLE) {
    var menu;

    if (ROLE === 'USER_ROLE') {

        menu = [{
                titulo: 'Recursos',
                icono: 'mdi mdi-gauge',
                submenu: [{
                    titulo: 'Dashboard',
                    url: '/dashboard'
                }]
            },
            {
                titulo: 'Casos',
                icono: 'mdi mdi-gauge',
                submenu: [{
                    titulo: 'Lista de Casos',
                    url: '/casos-list'
                }]
            },
            {
                titulo: 'Procesos',
                icono: 'mdi mdi-gauge',
                submenu: [

                    {
                        titulo: 'Analisis de Casos',
                        url: '/analisis-caso'
                    },
                    {
                        titulo: 'Avocatorias',
                        url: '/avocatorias'
                    },
                    {
                        titulo: 'Audiencias',
                        url: '/audiencias'
                    },
                    {
                        titulo: 'Resoluciones',
                        url: '/resoluciones'
                    }
                ]
            }, {
                titulo: 'Historicos',
                icono: 'mdi mdi-gauge',
                submenu: [{
                    titulo: 'Casos Historicos',
                    url: '/casos-historicos'
                }]
            }
        ];

    } else {

        if (ROLE === 'ADMIN_ROLE') {


            menu = [{
                titulo: 'Usuarios',
                icono: 'mdi-account',
                submenu: [{
                    titulo: 'Usuarios',
                    url: '/usuarios'
                }]
            }]
            /*  menu[0].submenu.unshift({
    
               titulo: 'Usuarios',
               url: '/usuarios'
           }) */
        }

    }


    return menu;
}