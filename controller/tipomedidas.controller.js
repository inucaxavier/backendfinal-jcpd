const db = require('../database/db.js');
const TiposMedidas = db.tiposmedidas;

exports.findAll = (req, res) => {
    TiposMedidas.findAll().then(tiposmedidas => {
        // Send all customers to Client
        res.send(tiposmedidas);
    });
};

// Find a Customer by Id
exports.findById = (req, res) => {
    const tme_id = req.params.tmedidasId;
    TiposMedidas.findById(tme_id).then(tiposmedidas => {
        res.send(tiposmedidas);
    })
};