const db = require('../database/db.js');
const Avocatoria = db.avocatoria;

// Post a Avocatoria
exports.create = (req, res) => {
    // Save to MySQL database
    Avocatoria.create({
        avo_id: req.body.avo_id,
        anc_id: req.body.anc_id,
        avo_fecha_conciliacion: req.body.avo_fecha_conciliacion,
        avo_observaciones: req.body.avo_observaciones,
        avo_hora_audiencia: req.body.avo_hora_audiencia

    }).then(t_avocatoria => {
        // Send created Avocatoria to client
        res.send(t_avocatoria);
    });
};
// FETCH all Avocatorias
/* exports.findAll = (req, res) => {
    Avocatoria.findAll().then(avocatoria => {
        // Send all Avocatorias to Client
        res.send(avocatoria);
    });
}; */
exports.findAll = async (req, res) => {

    await Avocatoria.findAll({}).then(avocatorias => {
        // Send all customers to Client
        res.status(200).json({
            ok: true,
            avocatorias
        })
    }).catch(error => console.log("no existen casos" + error))
};

// Find a Avocatoria by Id
exports.findById = (req, res) => {
    const avo_id = req.params.avocatoriaId;
    Avocatoria.findById(avo_id).then(avocatoria => {
        res.send(avocatoria);
    })
};

// Update a Avocatoria
exports.update = (req, res) => {
    const avo_id = req.params.avocatoriaId;
    Avocatoria.update({

            anc_id: req.body.anc_id,
            avo_fecha_conciliacion: req.body.avo_fecha_conciliacion,
            avo_observaciones: req.body.avo_observaciones,
            avo_hora_audiencia: req.body.avo_hora_audiencia

        },

        {
            where: {
                avo_id: req.params.avocatoriaId
            }
        }).then(() => {
        res.status(200).send("updated successfully a caso with avo_id = " + avo_id);
    });
};

// Delete a Avocatoria by Id
exports.delete = (req, res) => {
    const avo_id = req.params.avocatoriaId;
    Avocatoria.destroy({
        where: {
            avo_id: avo_id
        }
    }).then(() => {
        res.status(200).send('deleted successfully a caso with id = ' + avo_id);
    });
};
// Delete a Avocatoria by Id