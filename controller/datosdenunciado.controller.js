const db = require('../database/db.js');
const DatosDenunciado = db.datosdenunciado;

// Post a Customer
exports.create = (req, res) => {
    // Save to MySQL database
    DatosDenunciado.create({
        caso_id: req.body.caso_id,
        dag_nombre: req.body.dag_nombre,
        dag_apellido: req.body.dag_apellido,
        dag_ci: req.body.dag_ci,
        dag_relacion_afectado: req.body.dag_relacion_afectado,
        tper_id: req.body.tper_id,
        gene_id: req.body.gene_id,
        iden_id: req.body.iden_id,
        prov_id: req.body.prov_id,
        can_id: req.body.can_id,
        sec_id: req.body.sec_id,
        parr_id: req.body.parr_id,
        dag_direccion: req.body.dag_direccion,
        dag_telefono: req.body.dag_telefono,
        dag_email: req.body.dag_email,
        dag_observacion: req.body.dag_observacion

    }).then(t_denunciado => {
        // Send created customer to client
        res.send(t_denunciado);
    });
};
// FETCH all Customers
exports.findAll = (req, res) => {
    DatosDenunciado.findAll().then(datosdenunciado => {
        // Send all customers to Client
        res.send(datosdenunciado);
    });
};

// Find a Customer by Id
exports.findById = (req, res) => {
    const dag_id = req.params.datosdenunciadoId;
    DatosDenunciado.findById(dag_id).then(datosdenunciado => {
        res.send(datosdenunciado);
    })
};
exports.findByIdCaso = (req, res) => {
    const caso_id = req.params.datosdenunciadoIdCaso;
    DatosDenunciado.findById(caso_id).then(datosdenunciado => {
        res.send(datosdenunciado);
    })
};


// Update a Customer
exports.update = (req, res) => {
    const dag_id = req.params.datosdenunciadoId;
    DatosDenunciado.update({

            iden_id: req.body.iden_id,
            gene_id: req.body.gene_id,
            prov_id: req.body.prov_id,
            sec_id: req.body.sec_id,
            can_id: req.body.can_id,
            tper_id: req.body.tper_id,
            parr_id: req.body.parr_id,
            dag_nombre: req.body.dag_nombre,
            dag_apellido: req.body.dag_apellido,
            dag_ci: req.body.dag_ci,
            dag_relacion_afectado: req.body.dag_relacion_afectado,
            dag_direccion: req.body.dag_direccion,
            dag_telefono: req.body.dag_telefono,
            dag_email: req.body.dag_email,
            dag_observacion: req.body.dag_observacion
        },

        {
            where: {
                dag_id: req.params.datosdenunciadoId
            }
        }).then(() => {
        res.status(200).send("updated successfully a datos denuncidado with dag_id = " + dag_id);
    });
};

// Delete a Customer by Id
exports.delete = (req, res) => {
    const dag_id = req.params.datosdenunciadoId;
    DatosDenunciado.destroy({
        where: {
            dag_id: dag_id
        }
    }).then(() => {
        res.status(200).send('deleted successfully a datos denuncidado with id = ' + dag_id);
    });
};
// Delete a Customer by Id