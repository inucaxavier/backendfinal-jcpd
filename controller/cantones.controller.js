const db = require('../database/db.js');
const Cantones = db.cantones;

exports.findAll = (req, res) => {
    Cantones.findAll().then(cantones => {
        // Send all customers to Client
        res.send(cantones);
    });
};

// Find a Customer by Id
exports.findById = (req, res) => {
    const can_id = req.params.canId;
    Cantones.findById(can_id).then(cantones => {
        res.send(cantones);
    })
};