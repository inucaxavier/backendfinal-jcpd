const db = require('../database/db.js');
const DatosAfectado = db.datosafectado;
const Caso = db.casos;

// Post a Customer
exports.create = (req, res) => {
    // Save to MySQL database
    DatosAfectado.create({
        caso_id: req.body.caso_id,
        daf_nombre: req.body.daf_nombre,
        daf_apellido: req.body.daf_apellido,
        daf_ci: req.body.daf_ci,
        daf_fecha_nacimiento: req.body.daf_fecha_nacimiento,
        daf_edad: req.body.daf_edad,
        iden_id: req.body.iden_id,
        gene_id: req.body.gene_id,
        dis_id: req.body.dis_id,
        prov_id: req.body.prov_id,
        can_id: req.body.can_id,
        parr_id: req.body.parr_id,
        daf_direccion: req.body.daf_direccion,
        daf_telefono: req.body.daf_telefono,
        daf_email: req.body.daf_email,
        daf_obsevacion: req.body.daf_obsevacion

    }).then(datosafectado => {
        // Send created customer to client
        res.status(200).json({
            ok: true,
            datosafectado: datosafectado
        })
    });

};
exports.createByIdRelacional = async (req, res, next) => {

    const caso = await Caso.findOne({
        where: {
            caso_numcaso: req.params.caso_numcaso
        }
    });

    const caso_id = caso.caso_id;
    const resultado = await DatosAfectado.create({
            caso_id,
            iden_id: req.body.iden_id,
            gene_id: req.body.gene_id,
            dis_id: req.body.dis_id,
            prov_id: req.body.prov_id

        })
        .then(t_datosafectado => {
            // Send created customer to client
            res.send(t_datosafectado);
        });
    if (!resultado) {
        return next();
    }
}



// FETCH all Customers
exports.findAll = (req, res) => {
    DatosAfectado.findAll().then(datosafectado => {
        // Send all customers to Client
        res.send(datosafectado);
    });
};

// Find a Customer by Id
exports.findById = (req, res) => {
    const daf_id = req.params.datosafectadoId;
    DatosAfectado.findById(daf_id).then(datosafectado => {
        res.send(datosafectado);
    })
};

// Update a Customer
exports.update = (req, res) => {
    const daf_id = req.params.datosafectadoId;
    DatosAfectado.update({
            iden_id: req.body.iden_id,
            gene_id: req.body.gene_id,
            dis_id: req.body.dis_id,
            prov_id: req.body.prov_id,
            can_id: req.body.can_id,
            sec_id: req.body.sec_id,
            parr_id: req.body.parr_id,
            daf_nombre: req.body.daf_nombre,
            daf_apellido: req.body.daf_apellido,
            daf_ci: req.body.daf_ci,
            daf_fecha_nacimiento: req.body.daf_fecha_nacimiento,
            daf_edad: req.body.daf_edad,
            daf_direccion: req.body.daf_direccion,
            daf_telefono: req.body.daf_telefono,
            daf_email: req.body.daf_email,
            daf_obsevacion: req.body.daf_obsevacion
        },

        {
            where: {
                daf_id: req.params.datosafectadoId
            }
        }).then(() => {
        res.status(200).send("updated successfully a datosfectado with daf_id = " + daf_id);
    });
};

// Delete a Customer by Id
exports.delete = (req, res) => {
    const daf_id = req.params.datosafectadoId;
    DatosAfectado.destroy({
        where: {
            daf_id: daf_id
        }
    }).then(() => {
        res.status(200).send('deleted successfully a datosfectado with id = ' + daf_id);
    });
};
// Delete a Customer by Id

exports.crearAfectado = (req, res) => {
    const {
        daf_nombre,
        daf_apellido,
        daf_ci,
        daf_fecha_nacimiento,
        daf_edad,
        iden_id,
        gene_id,
        dis_id,
        prov_id,
        can_id,
        parr_id,
        daf_direccion,
        daf_telefono,
        daf_email,
        daf_obsevacion
    } = req.body
    const {
        caso_id
    } = req.params
    return DatosAfectado
        .create({
            caso_id,
            daf_nombre,
            daf_apellido,
            daf_ci,
            daf_fecha_nacimiento,
            daf_edad,
            iden_id,
            gene_id,
            dis_id,
            prov_id,
            can_id,
            parr_id,
            daf_direccion,
            daf_telefono,
            daf_email,
            daf_obsevacion
        })
        .then(DatosAfectado => res.status(201).send({
            message: `Your book with the title has been created successfully `,
            DatosAfectado
        }))
}