const db = require('../database/db.js');
const Sectores = db.sectores;

// Post a Customer
exports.create = (req, res) => {
    // Save to MySQL database
    Sectores.create({

        sec_id: req.body.sec_id,
        parr_id: req.body.parr_id,
        sec_nombre: req.body.sec_nombre

    }).then(t_sectores => {
        // Send created customer to client
        res.send(t_sectores);
    });
};
// FETCH all Customers
exports.findAll = (req, res) => {
    Sectores.findAll().then(sectores => {
        // Send all customers to Client
        res.send(sectores);
    });
};

// Find a Customer by Id
exports.findById = (req, res) => {
    const sec_id = req.params.secId;
    Sectores.findById(sec_id).then(sectores => {
        res.send(sectores);
    })
};

// Update a Customer
exports.update = (req, res) => {
    const sec_id = req.params.secId;
    Sectores.update({

            parr_id: req.body.parr_id,
            sec_nombre: req.body.sec_nombre

        },

        { where: { sec_id: req.params.secId } }).then(() => {
        res.status(200).send("updated successfully a sectores with sec_id = " + sec_id);
    });
};

// Delete a Customer by Id
exports.delete = (req, res) => {
    const sec_id = req.params.secId;
    Sectores.destroy({
        where: { sec_id: sec_id }
    }).then(() => {
        res.status(200).send('deleted successfully a sectores with sec_id = ' + sec_id);
    });
};
// Delete a Customer by Id3