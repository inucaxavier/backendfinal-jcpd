const db = require('../database/db.js');
const TipoParroquia = db.tipoparroquia;

exports.findAll = (req, res) => {
    TipoParroquia.findAll().then(tipoparroquia => {
        // Send all customers to Client
        res.send(tipoparroquia);
    });
};

// Find a Customer by Id
exports.findById = (req, res) => {
    const tparr_id = req.params.tparroId;
    TipoParroquia.findById(tparr_id).then(tipoparroquia => {
        res.send(tipoparroquia);
    })
};