const db = require('../database/db.js');
const Discapacidad = db.discapacidad;

exports.findAll = (req, res) => {
    Discapacidad.findAll().then(discapacidad => {
        // Send all customers to Client
        res.send(discapacidad);
    });
};

// Find a Customer by Id
exports.findById = (req, res) => {
    const dis_id = req.params.disId;
    Discapacidad.findById(dis_id).then(discapacidad => {
        res.send(discapacidad);
    })
};