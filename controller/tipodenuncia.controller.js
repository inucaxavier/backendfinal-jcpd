const db = require('../database/db.js');
const TipoDenuncia = db.tipodenuncia;

exports.findAll = (req, res) => {
    TipoDenuncia.findAll().then(tipodenuncia => {
        // Send all customers to Client
        res.send(tipodenuncia);
    });
};

// Find a Customer by Id
exports.findById = (req, res) => {
    const tdenun_id = req.params.tdenunId;
    TipoDenuncia.findById(tdenun_id).then(tipodenuncia => {
        res.send(tipodenuncia);
    })
};