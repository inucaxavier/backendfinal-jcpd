const db = require('../database/db.js');
const DerechoAmenzado = db.derechoamenazado;

exports.findAll = (req, res) => {
    DerechoAmenzado.findAll().then(derechoamenazado => {
        // Send all customers to Client
        res.send(derechoamenazado);
    });
};

// Find a Customer by Id
exports.findById = (req, res) => {
    const dav_id = req.params.davId;
    DerechoAmenzado.findById(dav_id).then(derechoamenazado => {
        res.send(derechoamenazado);
    })
};