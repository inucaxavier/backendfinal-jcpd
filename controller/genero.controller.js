const db = require('../database/db.js');
const Genero = db.genero;

exports.findAll = (req, res) => {
    Genero.findAll().then(genero => {
        // Send all customers to Client
        res.send(genero);
    });
};

// Find a Customer by Id
exports.findById = (req, res) => {
    const gene_id = req.params.geneId;
    Genero.findById(gene_id).then(genero => {
        res.send(genero);
    })
};