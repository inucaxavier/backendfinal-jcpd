const db = require('../database/db.js');
const TipoDerechos = db.tipoderechos;

exports.findAll = (req, res) => {
    TipoDerechos.findAll().then(tipoderechos => {
        // Send all customers to Client
        res.send(tipoderechos);
    });
};

// Find a Customer by Id
exports.findById = (req, res) => {
    const tder_id = req.params.tderId;
    TipoDerechos.findById(tder_id).then(tipoderechos => {
        res.send(tipoderechos);
    })
};