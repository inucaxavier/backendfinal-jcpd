const db = require('../database/db.js');
const RevisionAudienciaPrueba = db.revisionAudienciaPrueba;

// Post a Customer
exports.create = (req, res) => {
    // Save to MySQL database
    RevisionAudienciaPrueba.create({

        raup_id: req.body.raup_id,
        tme_id: req.body.tme_id,
        caso_id: req.body.caso_id,
        raup_fecha_avocatoria_caso: req.body.raup_fecha_avocatoria_caso,
        raup_fecha_resgistro_caso: req.body.raup_fecha_resgistro_caso,
        raup_fecha_audiencia_prueba: req.body.raup_fecha_audiencia_prueba,
        raup_fecha_audi_conciliacion: req.body.raup_fecha_audi_conciliacion,
        raup_hora_audiencia_prueba: req.body.raup_hora_audiencia_prueba,
        raup_resumen_resolucion: req.body.raup_resumen_resolucion,
        raup_resumen_audiencia: req.body.raup_resumen_audiencia,
        raup_fecha_inicio_medida: req.body.raup_fecha_inicio_medida,
        raup_fecha_conclucion_medida: req.body.raup_fecha_conclucion_medida,
        raup_responsable: req.body.raup_responsable

    }).then(t_revision_audiencia_prueba => {
        // Send created customer to client
        res.send(t_revision_audiencia_prueba);
    });
};
// FETCH all Customers
exports.findAll = (req, res) => {
    RevisionAudienciaPrueba.findAll().then(revisionAudienciaPrueba => {
        // Send all customers to Client
        res.send(revisionAudienciaPrueba);
    });
};

// Find a Customer by Id
exports.findById = (req, res) => {
    const raup_id = req.params.revaudipId;
    RevisionAudienciaPrueba.findById(raup_id).then(revisionAudienciaPrueba => {
        res.send(revisionAudienciaPrueba);
    })
};

// Update a Customer
exports.update = (req, res) => {
    const raup_id = req.params.revaudipId;
    RevisionAudienciaPrueba.update({

            tme_id: req.body.tme_id,
            caso_id: req.body.caso_id,
            raup_fecha_avocatoria_caso: req.body.raup_fecha_avocatoria_caso,
            raup_fecha_resgistro_caso: req.body.raup_fecha_resgistro_caso,
            raup_fecha_audiencia_prueba: req.body.raup_fecha_audiencia_prueba,
            raup_fecha_audi_conciliacion: req.body.raup_fecha_audi_conciliacion,
            raup_hora_audiencia_prueba: req.body.raup_hora_audiencia_prueba,
            raup_resumen_resolucion: req.body.raup_resumen_resolucion,
            raup_resumen_audiencia: req.body.raup_resumen_audiencia,
            raup_fecha_inicio_medida: req.body.raup_fecha_inicio_medida,
            raup_fecha_conclucion_medida: req.body.raup_fecha_conclucion_medida,
            raup_responsable: req.body.raup_responsable

        },

        { where: { raup_id: req.params.revaudipId } }).then(() => {
        res.status(200).send("updated successfully a revision audiencia prueba with raup_id = " + raup_id);
    });
};

// Delete a Customer by Id
exports.delete = (req, res) => {
    const raup_id = req.params.revaudipId;
    RevisionAudienciaPrueba.destroy({
        where: { raup_id: raup_id }
    }).then(() => {
        res.status(200).send('deleted successfully a revision audiencia prueba with raup_id = ' + raup_id);
    });
};
// Delete a Customer by Id