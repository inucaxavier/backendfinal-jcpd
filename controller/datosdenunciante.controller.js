const db = require('../database/db.js');
const DatosDenunciante = db.datosdenunciante;

// Post a Customer
exports.create = (req, res) => {
    // Save to MySQL database
    DatosDenunciante.create({
        caso_id: req.body.caso_id,
        dden_id: req.body.dden_id,
        tper_id: req.body.tper_id,
        iden_id: req.body.iden_id,
        gene_id: req.body.gene_id,
        prov_id: req.body.prov_id,
        parr_id: req.body.parr_id,
        can_id: req.body.can_id,
        sec_id: req.body.sec_id,
        dden_nombre: req.body.dden_nombre,
        dden_apellido: req.body.dden_apellido,
        dden_ci: req.body.dden_ci,
        dden_fecha_nacimento: req.body.dden_fecha_nacimento,
        dden_edad: req.body.dden_edad,
        dden_relacion_afectado: req.body.dden_relacion_afectado,
        dden_direccion: req.body.dden_direccion,
        dden_telefono: req.body.dden_telefono,
        dden_email: req.body.dden_email,
        dden_observaciones: req.body.dden_observaciones

    }).then(t_denunciante => {
        // Send created customer to client
        res.send(t_denunciante);
    });
};
// FETCH all Customers
exports.findAll = (req, res) => {
    DatosDenunciante.findAll().then(datosdenunciante => {
        // Send all customers to Client
        res.send(datosdenunciante);
    });
};
exports.incrementando = (req, res) => {
    DatosDenunciante.findAll({
        where: {
            parr_id: '571'
        }
    }).then(datosdenunciante => {
        // Send all customers to Client
        res.status(200).json({
            ok: true,
            datosdenunciante
        })
    }).catch(error => console.log("no existen casos" + error))
};

// Find a Customer by Id
exports.findById = (req, res) => {
    const dden_id = req.params.datosdenuncianteId;
    DatosDenunciante.findById(dden_id).then(datosdenunciante => {
        res.send(datosdenunciante);
    })
};

// Update a Customer
exports.update = (req, res) => {
    const dden_id = req.params.datosdenuncianteId;
    DatosDenunciante.update({
            tper_id: req.body.tper_id,
            iden_id: req.body.iden_id,
            gene_id: req.body.gene_id,
            prov_id: req.body.prov_id,
            parr_id: req.body.parr_id,
            can_id: req.body.can_id,
            sec_id: req.body.sec_id,
            dden_nombre: req.body.dden_nombre,
            dden_apellido: req.body.dden_apellido,
            dden_ci: req.body.dden_ci,
            dden_fecha_nacimento: req.body.dden_fecha_nacimento,
            dden_edad: req.body.dden_edad,
            dden_relacion_afectado: req.body.dden_relacion_afectado,
            dden_direccion: req.body.dden_direccion,
            dden_telefono: req.body.dden_telefono,
            dden_email: req.body.dden_email,
            dden_observaciones: req.body.dden_observaciones
        },

        {
            where: {
                dden_id: req.params.datosdenuncianteId
            }
        }).then(() => {
        res.status(200).send("updated successfully a datos denunciante with dden_id = " + dden_id);
    });
};

// Delete a Customer by Id
exports.delete = (req, res) => {
    const dden_id = req.params.datosdenuncianteId;
    DatosDenunciante.destroy({
        where: {
            dden_id: dden_id
        }
    }).then(() => {
        res.status(200).send('deleted successfully a datos denunciante with dden_id = ' + dden_id);
    });
};
// Delete a Customer by Id