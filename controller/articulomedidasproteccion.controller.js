const db = require('../database/db.js');
const ArtMedPro = db.artmedpro;

exports.findAll = (req, res) => {
    ArtMedPro.findAll().then(artmedpro => {
        // Send all customers to Client
        res.send(artmedpro);
    });
};

// Find a Customer by Id
exports.findById = (req, res) => {
    const mpa_id = req.params.mpaId;
    ArtMedPro.findById(mpa_id).then(artmedpro => {
        res.send(artmedpro);
    })
};