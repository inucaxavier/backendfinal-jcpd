const db = require('../database/db.js');
const usuario = db.user;
const Casos = db.casos;
const proco = db.proco;
const tdamenaza = db.tderamenazas;
const deramenza = db.derechoamenazado;
const DatosAfectado = db.datosafectado;
const DatosDenunciado = db.datosdenunciado;
const DatosDenunciante = db.datosdenunciante;
const parroquia = db.parroquias;
const sequelize = db.sequelize;
const identidad = db.identidad;
const gene = db.genero;
const dis = db.discapacidad;
const tper = db.tipopersona;
const tder = db.tipoderechos;
const analisiscasos = db.analisisCasos;
const tipocasos = db.tipocasos;
const avocatoria = db.avocatoria;
const revisionAudienciaPrueba = db.revisionAudienciaPrueba;
const tipoMedidas = db.tiposmedidas;
const artmedidapro = db.artmedpro;
const audiencia = db.audiencia;
const resolucion = db.resolucion;
const archivos = db.files;
const tipoDenuncia = db.tipodenuncia;
// Post a Customer
exports.create = async (req, res) => {

    // Save to MySQL database
    await Casos.create({
            caso_id: req.body.caso_id,
            proco_id: req.body.proco_id,
            tda_id: req.body.tda_id,
            dav_id: req.body.dav_id,
            tdenun_id: req.body.tdenun_id,
            caso_numcaso: req.body.caso_numcaso,
            caso_motivo: req.body.caso_motivo,
            caso_observaciones: req.body.caso_observaciones,
            caso_fechareg: req.body.caso_fechareg,
            caso_fechaingreso: req.body.caso_fechaingreso,
            viewCount: req.body.viewCount,
            published: req.body.published

        }).then(casos => {
            // Send created customer to client
            res.status(200).json({
                ok: true,
                casos: casos,
                message: 'Agregado Correctamente'
            })
        })
        .then(() => console.log('\x1b[32m%s\x1b[0m', 'Insertado correctamente')).catch(error => console.error(error));
};
// FETCH all Customers
exports.findAll = async (req, res) => {

    await Casos.findAll({
        order: sequelize.literal("caso_id DESC"),
        where: {
            published: true
        }
    }).then(casos => {
        // Send all customers to Client
        res.status(200).json({
            ok: true,
            casos
        })
    }).catch(error => console.log("no existen casos" + error))
};

exports.encontrarHistoricos = async (req, res) => {

    await Casos.findAll({
        order: sequelize.literal("caso_id DESC"),
        where: {
            published: false
        }
    }).then(casos => {
        // Send all customers to Client
        res.status(200).json({
            ok: true,
            casos
        })
    }).catch(error => console.log("no existen casos" + error))
};


exports.findAllUltimo = async (req, res) => {

    await Casos.findAll({
        order: sequelize.literal("caso_id DESC"),
        limit: 1,
        where: {
            published: true
        }
    }).then(casos => {
        // Send all customers to Client
        res.send(casos);
    }).catch(error => console.log("no existen casos" + error))
};

// Find a Customer by Id
exports.findByCaso = async function (req, res) {

    await Casos.findOne({
        where: {
            caso_numcaso: req.params.caso_numcaso,
            published: true
        },
        include: [{
                order: sequelize.literal("id DESC"),
                model: archivos,
                attributes: ['id', 'name']
            },
            {
                model: proco
            },
            {
                model: tdamenaza,
                include: [{
                    model: tder
                }]
            },
            {
                model: deramenza
            },
            {
                model: tipoDenuncia
            },
            {
                model: analisiscasos,
                include: [{
                    model: tipocasos
                }, {
                    model: avocatoria
                }]
            },
            {
                model: revisionAudienciaPrueba,
                include: [{
                    model: tipoMedidas,
                    include: [{
                        model: artmedidapro
                    }]
                }]
            },
            {
                model: audiencia
            },
            {
                model: resolucion,
                include: [{
                        model: audiencia
                    },
                    {
                        model: tipoMedidas
                    }
                ]
            },
            {
                model: DatosAfectado,
                include: [{
                        model: parroquia
                    },
                    {
                        model: identidad
                    },
                    {
                        model: gene
                    },
                    {
                        model: dis
                    }
                ]
            },
            {
                model: DatosDenunciado,
                include: [{
                    model: parroquia
                }, {
                    model: identidad
                }, {
                    model: gene
                }, {
                    model: tper
                }]
            },
            {
                model: DatosDenunciante,
                include: [{
                        model: parroquia
                    }, {
                        model: identidad
                    },
                    {
                        model: gene
                    }, {
                        model: tper
                    }
                ]
            }
        ]

    }).then(casos => {
        if (casos != null) {
            casos.update({
                viewCount: ++casos.viewCount
            })
        }
        res.send(casos);
    }).catch(error => console.log("no existe caso" + error));
};



exports.findByUsuario = async function (req, res) {

    await usuario.findOne({
        where: {
            id: req.params.id
        },
        include: [{
            model: Casos,
            where: {
                published: true
            },
            include: [{
                    model: DatosAfectado,
                    include: [{
                            model: parroquia
                        }, {
                            model: identidad
                        },
                        {
                            model: gene
                        }, {
                            model: dis
                        }
                    ]
                },
                {
                    model: DatosDenunciado,
                    include: [{
                        model: parroquia
                    }, {
                        model: identidad
                    }, {
                        model: gene
                    }, {
                        model: tper
                    }]
                },
                {
                    model: DatosDenunciante,
                    include: [{
                            model: parroquia
                        }, {
                            model: identidad
                        },
                        {
                            model: gene
                        }
                    ]
                }
            ]
        }]


    }).then(usuarios => {
        res.status(200).json({
            ok: true,
            usuarios
        })
    }).catch(error => console.log("no existe caso" + error));
};


// Update a Customer
exports.update = (req, res) => {
    const caso_id = req.params.casosId;
    Casos.update({

            proco_id: req.body.proco_id,
            tda_id: req.body.tda_id,
            dav_id: req.body.dav_id,
            tdenun_id: req.body.tdenun_id,
            caso_numcaso: req.body.caso_numcaso,
            caso_motivo: req.body.caso_motivo,
            caso_observaciones: req.body.caso_observaciones,
            caso_fechareg: req.body.caso_fechareg,
            caso_fechaingreso: req.body.caso_fechaingreso
        },

        {
            where: {
                caso_id: req.params.casosId
            }
        }).then(() => {
        res.status(200).send("updated successfully a caso with caso_id = " + caso_id);
    });
};

// Delete a Customer by Id
exports.delete = (req, res) => {
    const caso_id = req.params.casosId;
    Casos.findOne({
        where: {
            caso_id: caso_id
        }
    }).then(function (casos) {
        if (casos != null) {
            casos.destroy().then(
                res.status(200).send('deleted successfully a caso with id = ' + caso_id));
        } else {
            res.status(400).send({
                message: "Caso no puede ser borrado por que no existe"
            });
        }
    })
};



// Delete a Customer by Id

// dashboard services

exports.getDashboarOverviewCasos = function (req, res) {
    Casos.findAll({
        order: sequelize.literal("date DESC")
    }).then(casos => {
        // Send all customers to Client
        res.send(casos);
    });
};

exports.updatePublishState = function (req, res) {
    Casos.findOne({
        where: {
            caso_id: req.body.caso_id
        }
    }).then(function (casos) {
        if (casos != null) {
            casos.update({
                published: req.body.published
            });
        }
        res.send(casos);
    });
};
exports.editDashboardCasobyKey = function (req, res) {
    Casos.findOne({
        where: {
            caso_numcaso: req.params.caso_numcaso
        }
    }).then(casos => {
        // Send all customers to Client
        res.send(casos);
    });
};





exports.buscarAfectadoPorIdRelacional = async (req, res, next) => {
    const casosPromise = Casos.findAll();
    const casoPromise = Casos.findOne({
        where: {
            caso_numcaso: req.params.caso_numcaso
        }
    });

    const [casos, caso] = await Promise.all([casosPromise, casoPromise]);
    const denunciado = await DatosDenunciado.findAll();
    // consultal datoafectado de caso actual
    const afectado = await DatosAfectado.findAll({
        where: {
            caso_id: caso.caso_id
        },
        include: [{
            model: Casos
        }]
    }).then(afectado => res.send(afectado))
    return next();
}


exports.buscarAfectadoPorIdRelacional = async (req, res, next) => {
    const casosPromise = Casos.findAll();
    const casoPromise = Casos.findOne({
        where: {
            caso_numcaso: req.params.caso_numcaso
        }
    });

    const [casos, caso] = await Promise.all([casosPromise, casoPromise]);


    // consultal datoafectado de caso actual
    const afectado = await DatosAfectado.findAll({
        where: {
            caso_id: caso.caso_id
        },
        include: [{
            model: Casos
        }]
    }).then(afectado => res.send(afectado))
    return next();
}


/* exports.busqueda = (req, res, next) => {
    var busqueda = req.params.busqueda;
    var regex = RegExp(busqueda, 'i');


    buscarAfectados(busqueda, regex).then(afectado => {
        res.status(200).json({
            ok: true,
            afectado: afectado
        });
    });
} */

exports.busqueda = (req, res, next) => {

    var busqueda = req.params.busqueda;
    var regex = new $like

    DatosAfectado.findAll({
        where: {
            daf_nombre: regex
        }
    }).then(t_datosafectado => res.status(200).json({
        ok: true,
        t_datosafectado: t_datosafectado
    }))
    return next();
}



exports.getPostById = async (req, res) => {
    try {
        const {
            caso_id
        } = req.params;
        const post = await DatosAfectado.findOne({
            where: {
                caso_id: caso_id
            },
            include: [{
                model: Casos
            }]
        });
        if (post) {
            return res.status(200).json({
                post
            });
        }
        return res.status(404).send("Post with the specified ID does not exists");
    } catch (error) {
        return res.status(500).send(error.message);
    }

};

exports.busquedaTablas = (req, res) => {

    var tabla = req.params.tabla;
    var busqueda = req.params.busqueda;
    var regex = new RegExp(busqueda, 'i');

    if (promesas.hasOwnProperty(tabla)) {
        promesas[tabla](busqueda, regex).then((respuesta) => {
            res.status(200).json({
                ok: true,
                [tabla]: respuesta,
            });
        });
    } else {
        res.status(400).json({
            ok: true,
            mensaje: "La tabla no es valida.",
            error: {
                message: "Tipos de tabla/colección no validas"
            }
        });

    }
};