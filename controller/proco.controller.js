const db = require('../database/db.js');
const Proco = db.proco;

exports.findAll = (req, res) => {
    Proco.findAll().then(proco => {
        // Send all customers to Client
        res.send(proco);
    });
};

// Find a Customer by Id
exports.findById = (req, res) => {
    const proco_id = req.params.procoId;
    Proco.findById(proco_id).then(proco => {
        res.send(proco);
    })
};