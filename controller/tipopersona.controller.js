const db = require('../database/db.js');
const TipoPersona = db.tipopersona;

exports.findAll = (req, res) => {
    TipoPersona.findAll().then(tipopersona => {
        // Send all customers to Client
        res.send(tipopersona);
    });
};

// Find a Customer by Id
exports.findById = (req, res) => {
    const tper_id = req.params.tpersonaId;
    TipoPersona.findById(tper_id).then(tipopersona => {
        res.send(tipopersona);
    })
};