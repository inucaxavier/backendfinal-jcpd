const db = require('../database/db.js');
const Identidad = db.identidad;

exports.findAll = (req, res) => {
    Identidad.findAll().then(identidad => {
        // Send all customers to Client
        res.send(identidad);
    });
};

// Find a Customer by Id
exports.findById = (req, res) => {
    const iden_id = req.params.idenId;
    Identidad.findById(iden_id).then(identidad => {
        res.send(identidad);
    })
};