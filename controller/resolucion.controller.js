const db = require('../database/db.js');
const Resolucion = db.resolucion;

// Post a Customer
exports.create = (req, res) => {
    // Save to MySQL database
    Resolucion.create({

        res_id: req.body.res_id,
        rau_id: req.body.rau_id,
        tme_id: req.body.tme_id,
        caso_id: req.body.caso_id,
        res_resumen_resolucion: req.body.res_resumen_resolucion,
        res_fecha_inicio_media: req.body.res_fecha_inicio_media,
        res_fecha_conclucion_media: req.body.res_fecha_conclucion_media,
        res_responsable: req.body.res_responsable

    }).then(t_resolucion => {
        // Send created customer to client
        res.send(t_resolucion);
    });
};
// FETCH all Customers
exports.findAll = (req, res) => {
    Resolucion.findAll().then(resolucion => {
        // Send all customers to Client
        res.status(200).json({
            ok: true,
            resolucion
        })
    }).catch(error => console.log("no existen casos" + error))
};

// Find a Customer by Id
exports.findById = (req, res) => {
    const res_id = req.params.resId;
    Resolucion.findById(res_id).then(resolucion => {
        res.send(resolucion);
    })
};

// Update a Customer
exports.update = (req, res) => {
    const res_id = req.params.resId;
    Resolucion.update({

            rau_id: req.body.rau_id,
            tme_id: req.body.tme_id,
            caso_id: req.body.caso_id,
            res_resumen_resolucion: req.body.res_resumen_resolucion,
            res_fecha_inicio_media: req.body.res_fecha_inicio_media,
            res_fecha_conclucion_media: req.body.res_fecha_conclucion_media,
            res_responsable: req.body.res_responsable

        },

        {
            where: {
                res_id: req.params.resId
            }
        }).then(() => {
        res.status(200).send("updated successfully a resolucion with res_id = " + res_id);
    });
};

// Delete a Customer by Id
exports.delete = (req, res) => {
    const res_id = req.params.resId;
    Resolucion.destroy({
        where: {
            res_id: res_id
        }
    }).then(() => {
        res.status(200).send('deleted successfully a resolucion with res_id = ' + res_id);
    });
};
// Delete a Customer by Id3