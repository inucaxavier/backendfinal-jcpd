var stream = require('stream');

const db = require('../database/db.js');
const File = db.files;
const Caso = db.casos;

exports.uploadFile = (req, res) => {
	File.create({
		caso_id: req.body.caso_id,
		type: req.file.mimetype,
		name: req.file.originalname,
		data: req.file.buffer
	}).then(() => {
		res.json({
			msg: 'File uploaded successfully! -> filename = ' + req.file.originalname
		});
	})
}
exports.fileCaso = (req, res) => {
	File.create({
		caso_id: req.body.caso_id
	}).then(() => {
		res.json({
			msg: 'File uploaded successfully! -> filename '
		});
	})
}

exports.listAllFiles = (req, res) => {
	File.findAll({
		order: sequelize.literal("id DESC"),
		attributes: ['id', 'name']
	}).then(files => {
		res.json(files);
	});
}

exports.downloadFile = (req, res) => {
	File.findById(req.params.id).then(file => {
		var fileContents = Buffer.from(file.data, "base64");
		var readStream = new stream.PassThrough();
		readStream.end(fileContents);
		res.set('Content-disposition', 'attachment; filename=' + file.name);
		res.set('Content-Type', file.type);

		readStream.pipe(res);
	})
}

/* exports.subirArchivo = (req, res) => {

	File.create({
		type: req.file.mimetype,
		name: req.file.originalname,
		data: req.file.buffer
	}).then(archivo => {
		Caso.findAll({
			where: {

				caso_id: req.params.caso_id

			}
		}).then(casos => {
			res.json({
				casos,
				msg: 'File uploaded successfully! -> filename = ' + req.file.originalname
			});
		})
	}).catch(err => {
		res.json({
			"error": "Error -> " + err
		});
	});
} */