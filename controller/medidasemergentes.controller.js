const db = require('../database/db.js');
const MedidasEmergentes = db.medidasemergentes;

// Post a Customer
exports.create = (req, res) => {
    // Save to MySQL database
    MedidasEmergentes.create({

        me_id: req.body.me_id,
        tme_id: req.body.tme_id,
        me_fecha_aplicacion: req.body.me_fecha_aplicacion,
        me_fecha_inicio_medida: req.body.me_fecha_inicio_medida,
        me_fecha_conclucion_medida: req.body.me_fecha_conclucion_medida,
        avo_id: req.body.avo_id,
        me_responsable: req.body.me_responsable


    }).then(t_medidas_emergentes => {
        // Send created customer to client
        res.send(t_medidas_emergentes);
    });
};
// FETCH all Customers
exports.findAll = (req, res) => {
    MedidasEmergentes.findAll().then(medidasemergentes => {
        // Send all customers to Client
        res.send(medidasemergentes);
    });
};

// Find a Customer by Id
exports.findById = (req, res) => {
    const me_id = req.params.medidasemergentesId;
    MedidasEmergentes.findById(me_id).then(medidasemergentes => {
        res.send(medidasemergentes);
    })
};

// Update a Customer
exports.update = (req, res) => {
    const me_id = req.params.medidasemergentesId;
    MedidasEmergentes.update({

            tme_id: req.body.tme_id,
            me_fecha_aplicacion: req.body.me_fecha_aplicacion,
            me_fecha_inicio_medida: req.body.me_fecha_inicio_medida,
            me_fecha_conclucion_medida: req.body.me_fecha_conclucion_medida,
            avo_id: req.body.avo_id,
            me_responsable: req.body.me_responsable

        },

        { where: { me_id: req.params.medidasemergentesId } }).then(() => {
        res.status(200).send("updated successfully a medidas emergentes with me_id = " + me_id);
    });
};

// Delete a Customer by Id
exports.delete = (req, res) => {
    const me_id = req.params.medidasemergentesId;
    MedidasEmergentes.destroy({
        where: { me_id: me_id }
    }).then(() => {
        res.status(200).send('deleted successfully a medidas emergentes with me_id = ' + me_id);
    });
};
// Delete a Customer by Id