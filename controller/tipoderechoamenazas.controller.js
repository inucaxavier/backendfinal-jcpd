const db = require('../database/db.js');
const TipoDerechosAmenazas = db.tderamenazas;

exports.findAll = (req, res) => {
    TipoDerechosAmenazas.findAll().then(tderamenazas => {
        // Send all customers to Client
        res.send(tderamenazas);
    });
};

// Find a Customer by Id
exports.findById = (req, res) => {
    const tda_id = req.params.tderameId;
    TipoDerechosAmenazas.findById(tda_id).then(tderamenazas => {
        res.send(tderamenazas);
    })
};