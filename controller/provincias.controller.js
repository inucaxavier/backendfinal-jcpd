const db = require('../database/db.js');
const Provincias = db.provincias;

exports.findAll = (req, res) => {
    Provincias.findAll().then(provincias => {
        res.send(provincias);
    });
};
exports.findById = (req, res) => {
    const prov_id = req.params.provId;
    Provincias.findById(prov_id).then(provincias => {
        res.send(provincias);
    });
};