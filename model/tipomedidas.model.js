module.exports = (sequelize, Sequelize) => {
    const TipoMedidas = sequelize.define('t_tipos_medidas', {
        tme_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        tme_nombre: {
            type: Sequelize.STRING
        },
        mpa_id: {
            type: Sequelize.INTEGER
        }

    }, {
        timestamps: false,
        freezeTableName: true
    });

    return TipoMedidas;
}