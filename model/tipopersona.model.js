module.exports = (sequelize, Sequelize) => {
    const TipoPersona = sequelize.define('t_tipo_persona', {
        tper_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        tper_nombre: {
            type: Sequelize.STRING
        }

    }, {
        timestamps: false,
        freezeTableName: true
    });

    return TipoPersona;
}