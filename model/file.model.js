module.exports = (sequelize, Sequelize) => {
	const File = sequelize.define('file', {
		caso_id: {
			type: Sequelize.INTEGER
		},
		type: {
			type: Sequelize.STRING
		},
		name: {
			type: Sequelize.STRING
		},
		data: {
			type: Sequelize.BLOB('long')
		}
	});
	return File;
}