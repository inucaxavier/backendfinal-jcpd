module.exports = (sequelize, Sequelize) => {
    const DerechoAmenazadoV = sequelize.define('t_derecho_amenazado_violentado', {
        dav_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        dav_nombre: {
            type: Sequelize.STRING
        }
    }, {
        timestamps: false,
        freezeTableName: true
    });

    return DerechoAmenazadoV;
}