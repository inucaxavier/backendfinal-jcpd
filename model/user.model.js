module.exports = (sequelize, Sequelize) => {

	var rolesValidos = {
		values: ['ADMIN_ROLE', 'USER_ROLE'],
		message: '{VALUE} no es un rol permitido'
	}
	const User = sequelize.define('t_usuarios', {

		usu_nombres: {
			type: Sequelize.STRING,
			required: [true, 'El nombre es necesario']
		},
		usu_apellidos: {
			type: Sequelize.STRING,
			required: [true, 'El apellido es necesario']
		},
		usu_cedula: {
			type: Sequelize.STRING,
			required: [true, 'La cedula es necesario']
		},
		usu_email: {
			type: Sequelize.STRING,
			unique: true,
			required: [true, 'El correo es necesario'],
			isEmail: true,
			enum: rolesValidos
		},
		usu_password: {
			type: Sequelize.STRING,
			required: [true, 'La contraseña es necesaria']
		},
		usu_rol: {
			type: Sequelize.STRING,
			required: true,
			defaultValue: 'USER_ROLE'
		},
	}, {
		timestamps: false,
		freezeTableName: true
	});
	return User;
}