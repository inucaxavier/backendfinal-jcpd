module.exports = (sequelize, Sequelize) => {
    const Canton = sequelize.define(
        't_cantones', {
            can_id: {
                type: Sequelize.INTEGER,
                primaryKey: true
            },
            prov_id: {
                type: Sequelize.INTEGER
            },
            can_nombre: {
                type: Sequelize.STRING
            }

        }, {
            timestamps: false,
            freezeTableName: true
        });
    return Canton;
}