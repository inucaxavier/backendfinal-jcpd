module.exports = (sequelize, Sequelize) => {
    const Parroquia = sequelize.define('t_parroquias', {

        parr_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true

        },
        tparr_id: {
            type: Sequelize.INTEGER
        },
        can_id: {
            type: Sequelize.INTEGER
        },
        parr_nombre: {
            type: Sequelize.STRING
        }

    }, {
        timestamps: false,
        freezeTableName: true
    });
    return Parroquia;

}