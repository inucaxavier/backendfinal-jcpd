module.exports = (sequelize, Sequelize) => {
    const TipoParroquia = sequelize.define('t_tipo_parroquias', {
        tparr_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        tparr_nombre: {
            type: Sequelize.STRING
        }

    }, {
        timestamps: false,
        freezeTableName: true
    });

    return TipoParroquia;
}