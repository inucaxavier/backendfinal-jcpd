module.exports = (sequelize, Sequelize) => {
    const Identidad = sequelize.define('t_identidad', {
        iden_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        iden_nombre: {
            type: Sequelize.STRING
        }

    }, {
        timestamps: false,
        freezeTableName: true
    });

    return Identidad;
}