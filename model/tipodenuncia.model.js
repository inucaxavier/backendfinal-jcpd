module.exports = (sequelize, Sequelize) => {
    const TipoDenuncia = sequelize.define('t_tipo_denuncia', {
        tdenun_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        tdenun_nombre: {
            type: Sequelize.STRING
        }

    }, {
        timestamps: false,
        freezeTableName: true
    });

    return TipoDenuncia;
}