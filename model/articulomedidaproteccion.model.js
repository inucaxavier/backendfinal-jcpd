module.exports = (sequelize, Sequelize) => {
    const ArtMedPro = sequelize.define('t_medidas_proteccion_art', {
        mpa_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        mpa_nombre: {
            type: Sequelize.STRING
        }

    }, {
        timestamps: false,
        freezeTableName: true
    });

    return ArtMedPro;
}