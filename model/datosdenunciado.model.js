module.exports = (sequelize, Sequelize) => {
    const DatosDenunciado = sequelize.define('t_denunciado', {
        dag_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        caso_id: Sequelize.INTEGER,
        dag_nombre: Sequelize.STRING,
        dag_apellido: Sequelize.STRING,
        dag_ci: Sequelize.STRING,
        dag_relacion_afectado: Sequelize.STRING,
        tper_id: Sequelize.INTEGER,
        gene_id: Sequelize.INTEGER,
        iden_id: Sequelize.INTEGER,
        prov_id: Sequelize.INTEGER,
        can_id: Sequelize.INTEGER,
        sec_id: Sequelize.INTEGER,
        parr_id: Sequelize.INTEGER,
        dag_direccion: Sequelize.STRING,
        dag_telefono: Sequelize.STRING,
        dag_email: Sequelize.STRING,
        dag_observacion: Sequelize.STRING
    }, {
        timestamps: false,
        freezeTableName: true
    });

    return DatosDenunciado;
}