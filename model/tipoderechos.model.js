module.exports = (sequelize, Sequelize) => {
    const TipoDerechos = sequelize.define('t_tipos_derechos', {
        tder_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        tder_nombre: {
            type: Sequelize.STRING
        }

    }, {
        timestamps: false,
        freezeTableName: true
    });

    return TipoDerechos;
}