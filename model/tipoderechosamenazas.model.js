module.exports = (sequelize, Sequelize) => {
    const TipoDerechoAmenazas = sequelize.define('tipo_derecho_amenazas', {
        tda_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        tder_id: {
            type: Sequelize.INTEGER
        },
        tda_nombre: {
            type: Sequelize.STRING
        }

    }, {
        timestamps: false,
        freezeTableName: true
    });

    return TipoDerechoAmenazas;
}