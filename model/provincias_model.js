module.exports = (sequelize, Sequelize) => {
    const Provincia = sequelize.define('t_provincias', {
        prov_id: {
            type: Sequelize.INTEGER,
            primaryKey: true
        },
        prov_nombre: {
            type: Sequelize.STRING
        }
    }, {
        timestamps: false,
        freezeTableName: true
    });
    return Provincia;
}