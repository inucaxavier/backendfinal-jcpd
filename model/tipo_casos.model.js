module.exports = (sequelize, Sequelize) => {
    const TipoCasos = sequelize.define('t_tipo_casos', {
        tica_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        tica_nombre: {
            type: Sequelize.STRING
        }

    }, {
        timestamps: false,
        freezeTableName: true
    });

    return TipoCasos;
}