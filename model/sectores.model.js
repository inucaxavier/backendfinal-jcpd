module.exports = (sequelize, Sequelize) => {
    const Sectores = sequelize.define('t_sectores', {

        sec_id: {
            type: Sequelize.INTEGER,
            primaryKey: true
        },
        parr_id: {
            type: Sequelize.INTEGER
        },
        sec_nombre: {
            type: Sequelize.STRING
        }

    }, {
        timestamps: false,
        freezeTableName: true
    });
    return Sectores;

}