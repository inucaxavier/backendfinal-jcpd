module.exports = (sequelize, Sequelize) => {
    const MedidasEmergentes = sequelize.define('t_medidas_emergentes', {
        me_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        tme_id: {
            type: Sequelize.INTEGER
        },
        me_fecha_aplicacion: {
            type: Sequelize.DATE
        },
        me_fecha_inicio_medida: {
            type: Sequelize.DATE
        },
        me_fecha_conclucion_medida: {
            type: Sequelize.DATE
        },
        avo_id: {
            type: Sequelize.INTEGER
        },
        me_responsable: {
            type: Sequelize.STRING
        }
    }, {
        timestamps: false,
        freezeTableName: true
    });

    return MedidasEmergentes;
}