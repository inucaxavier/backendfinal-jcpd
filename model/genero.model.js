module.exports = (sequelize, Sequelize) => {
    const Genero = sequelize.define('t_genero', {
        gene_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        gene_nombre: {
            type: Sequelize.STRING
        }

    }, {
        timestamps: false,
        freezeTableName: true
    });

    return Genero;
}