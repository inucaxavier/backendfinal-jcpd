module.exports = (sequelize, Sequelize) => {
    const DatosAfectado = sequelize.define('t_datosafectado', {
        daf_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        caso_id: Sequelize.INTEGER,
        daf_nombre: Sequelize.STRING(50),
        daf_telefono: Sequelize.STRING,
        daf_apellido: Sequelize.STRING(50),
        daf_ci: Sequelize.STRING,
        daf_fecha_nacimiento: Sequelize.DATE,
        daf_edad: Sequelize.INTEGER,
        iden_id: Sequelize.INTEGER,
        gene_id: Sequelize.INTEGER,
        dis_id: Sequelize.INTEGER,
        prov_id: Sequelize.INTEGER,
        can_id: Sequelize.INTEGER,
        parr_id: Sequelize.INTEGER,
        daf_direccion: Sequelize.STRING,
        daf_email: Sequelize.STRING,
        daf_obsevacion: Sequelize.STRING

    }, {
        timestamps: false,
        freezeTableName: true
    });

    return DatosAfectado;
}