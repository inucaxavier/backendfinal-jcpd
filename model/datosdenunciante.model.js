module.exports = (sequelize, Sequelize) => {
    const DatosDenunciante = sequelize.define('t_denunciante', {
        dden_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        caso_id: Sequelize.INTEGER,
        dden_nombre: Sequelize.STRING,
        dden_apellido: Sequelize.STRING,
        dden_ci: Sequelize.STRING,
        dden_relacion_afectado: Sequelize.STRING,
        dden_fecha_nacimento: Sequelize.DATE,
        dden_edad: Sequelize.INTEGER,
        tper_id: Sequelize.INTEGER,
        gene_id: Sequelize.INTEGER,
        iden_id: Sequelize.INTEGER,
        prov_id: Sequelize.INTEGER,
        can_id: Sequelize.INTEGER,
        sec_id: Sequelize.INTEGER,
        parr_id: Sequelize.INTEGER,
        dden_direccion: Sequelize.STRING,
        dden_telefono: Sequelize.STRING,
        dden_email: Sequelize.STRING,
        dden_observaciones: Sequelize.STRING,
        parrosum: Sequelize.INTEGER
    }, {
        timestamps: false,
        freezeTableName: true
    });

    return DatosDenunciante;
}