module.exports = (sequelize, Sequelize) => {
    const Resolucion = sequelize.define('t_resolucion', {
        res_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        rau_id: {
            type: Sequelize.INTEGER
        },
        tme_id: {
            type: Sequelize.INTEGER
        },
        caso_id: {
            type: Sequelize.INTEGER
        },
        res_resumen_resolucion: {
            type: Sequelize.STRING
        },
        res_fecha_inicio_media: {
            type: Sequelize.DATE
        },
        res_fecha_conclucion_media: {
            type: Sequelize.DATE
        },
        res_responsable: {
            type: Sequelize.STRING
        }
    }, {
        timestamps: false,
        freezeTableName: true
    });

    return Resolucion;
}