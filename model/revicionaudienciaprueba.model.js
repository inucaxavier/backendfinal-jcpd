module.exports = (sequelize, Sequelize) => {
    const RevisionAudienciaPrueba = sequelize.define('t_revision_audiencia_prueba', {
        raup_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        tme_id: {
            type: Sequelize.INTEGER
        },
        caso_id: {
            type: Sequelize.INTEGER
        },
        raup_fecha_avocatoria_caso: {
            type: Sequelize.DATE
        },
        raup_fecha_resgistro_caso: {
            type: Sequelize.DATE
        },
        raup_fecha_audiencia_prueba: {
            type: Sequelize.DATE
        },
        raup_fecha_audi_conciliacion: {
            type: Sequelize.DATE
        },
        raup_hora_audiencia_prueba: {
            type: Sequelize.STRING
        },
        raup_resumen_resolucion: {
            type: Sequelize.STRING
        },
        raup_resumen_audiencia: {
            type: Sequelize.STRING
        },
        raup_fecha_inicio_medida: {
            type: Sequelize.DATE
        },
        raup_fecha_conclucion_medida: {
            type: Sequelize.DATE
        },
        raup_responsable: {
            type: Sequelize.STRING
        }
    }, {
        timestamps: false,
        freezeTableName: true
    });

    return RevisionAudienciaPrueba;
}