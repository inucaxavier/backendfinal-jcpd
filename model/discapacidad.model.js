module.exports = (sequelize, Sequelize) => {
    const Discapacidad = sequelize.define('t_discapacidad', {
        dis_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        dis_nombre: {
            type: Sequelize.STRING
        }

    }, {
        timestamps: false,
        freezeTableName: true
    });

    return Discapacidad;
}