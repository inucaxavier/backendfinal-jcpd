const express = require('express');
const cors = require('cors');
const bodyParser = require("body-parser");
const db = require('./database/db');
const app = express();
const port = process.env.PORT || 8080


var corsOptions = {
    origin: ["http://localhost:4200", "http://localhost:4300", "http://127.0.0.1:8081/"]
}
app.use(bodyParser.json())
app.use(cors())
app.use(
    bodyParser.urlencoded({
        extended: false
    })
)

require('./routes/router.js')(app);

// force: true will drop the table if it already exists
db.sequelize.sync({}).then(() => console.log('Conectado al servido')).catch(error => console.error(error));



app.listen(port, function () {
    console.log('Server is running on port: ' + port + '\x1b[32m%s\x1b[0m', ' Online');
    db.init();
})