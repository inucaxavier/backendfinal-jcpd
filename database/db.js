const Sequelize = require("sequelize");
const Op = Sequelize.Op;
const db = {};
const sequelize = new Sequelize("db_otavaloJcpd", "root", "", {
    host: "localhost",
    dialect: "mysql",
    port: 3307,
    operatorsAliases: Op, // use Sequelize.Op
    pool: {
        max: 5,
        min: 0,
        idle: 10000
    },
    dialectOptions: {
        timezone: process.env.db_timezone
    }
});

init = function () {
    sequelize
        .authenticate()
        .then(() => {
            console.log("Coneccion ha sido establecida exitosamente");

        }).catch(
            err => {
                console.log("Unable to connect to the database:", err);

            });

}
db.sequelize = sequelize
db.Sequelize = Sequelize

db.user = require('../model/user.model.js')(sequelize, Sequelize);
db.parroquias = require('../model/parroquias.model.js')(sequelize, Sequelize);
db.casos = require('../model/casos.model.js')(sequelize, Sequelize);
db.datosafectado = require('../model/datosafectado.model.js')(sequelize, Sequelize);
db.datosdenunciado = require('../model/datosdenunciado.model.js')(sequelize, Sequelize);
db.datosdenunciante = require('../model/datosdenunciante.model.js')(sequelize, Sequelize);


db.identidad = require('../model/identidad.model.js')(sequelize, Sequelize);
db.genero = require('../model/genero.model.js')(sequelize, Sequelize);
db.discapacidad = require('../model/discapacidad.model.js')(sequelize, Sequelize);
db.tipopersona = require('../model/tipopersona.model.js')(sequelize, Sequelize);
db.proco = require('../model/proco.model.js')(sequelize, Sequelize);
db.tderamenazas = require('../model/tipoderechosamenazas.model.js')(sequelize, Sequelize);
db.derechoamenazado = require('../model/derechosamenzado.model.js')(sequelize, Sequelize);
db.tipodenuncia = require('../model/tipodenuncia.model.js')(sequelize, Sequelize);
db.tipoderechos = require('../model/tipoderechos.model.js')(sequelize, Sequelize);


//====================================
// PROCESO
//====================================
db.analisisCasos = require('../model/analisis_casos.model.js')(sequelize, Sequelize);
db.tipocasos = require('../model/tipo_casos.model.js')(sequelize, Sequelize);
db.avocatoria = require('../model/avocatoria.model.js')(sequelize, Sequelize);
db.revisionAudienciaPrueba = require('../model/revicionaudienciaprueba.model')(sequelize, Sequelize);
db.tiposmedidas = require('../model/tipomedidas.model.js')(sequelize, Sequelize);
db.artmedpro = require('../model/articulomedidaproteccion.model.js')(sequelize, Sequelize);
db.audiencia = require('../model/audiencia.model.js')(sequelize, Sequelize);
db.resolucion = require('../model/resolucion.model.js')(sequelize, Sequelize);
db.files = require('../model/file.model.js')(sequelize, Sequelize);


//====================================
//ARCHIVOS
//====================================

/* db.files.belongsToMany(db.casos, {
    through: 'casos_files',
    foreignKey: 'casoId',
    otherKey: 'fileId'
});
db.casos.belongsToMany(db.files, {
    through: 'casos_files',
    foreignKey: 'fileId',
    otherKey: 'casoId'
});
 */
//====================================
//  RELACION DATOS CASOS
//====================================

db.casos.hasMany(db.files, {
    foreignKey: 'caso_id'
});
db.files.belongsTo(db.casos, {
    foreignKey: 'caso_id'
});
db.user.hasMany(db.casos, {
    foreignKey: 'id'
});

db.casos.hasMany(db.datosafectado, {
    foreignKey: 'caso_id'
});
db.datosafectado.belongsTo(db.casos, {
    foreignKey: 'caso_id'
});

db.casos.hasMany(db.datosdenunciado, {
    foreignKey: 'caso_id'
});
db.casos.hasMany(db.datosdenunciante, {
    foreignKey: 'caso_id'
});
db.casos.hasMany(db.analisisCasos, {
    foreignKey: 'caso_id'
});
db.analisisCasos.hasMany(db.avocatoria, {
    foreignKey: 'anc_id'
});
db.analisisCasos.belongsTo(db.tipocasos, {
    foreignKey: 'tica_id'
});

db.casos.hasMany(db.audiencia, {
    foreignKey: 'caso_id'
});

db.casos.hasMany(db.resolucion, {
    foreignKey: 'caso_id'
});
db.tiposmedidas.hasMany(db.resolucion, {
    foreignKey: 'tme_id'
})
db.resolucion.belongsTo(db.tiposmedidas, {
    foreignKey: 'tme_id'
})
db.audiencia.hasMany(db.resolucion, {
    foreignKey: 'rau_id'
})
db.resolucion.belongsTo(db.audiencia, {
    foreignKey: 'rau_id'
})

db.casos.hasMany(db.revisionAudienciaPrueba, {
    foreignKey: 'caso_id'
});
db.tiposmedidas.hasMany(db.revisionAudienciaPrueba, {
    foreignKey: 'tme_id'
});
db.revisionAudienciaPrueba.belongsTo(db.tiposmedidas, {
    foreignKey: 'tme_id'
});
db.tiposmedidas.belongsTo(db.artmedpro, {
    foreignKey: 'mpa_id'
});




db.casos.belongsTo(db.proco, {
    foreignKey: 'proco_id'
});
db.casos.belongsTo(db.tderamenazas, {
    foreignKey: 'tda_id'
});
db.tderamenazas.belongsTo(db.tipoderechos, {
    foreignKey: 'tder_id'
})

db.casos.belongsTo(db.derechoamenazado, {
    foreignKey: 'dav_id'
});
db.casos.belongsTo(db.tipodenuncia, {
    foreignKey: 'tdenun_id'
});

//====================================
//RELACION DATOS AFECTADOS
//====================================
db.parroquias.hasOne(db.datosafectado, {
    foreignKey: 'parr_id',
});
db.datosafectado.belongsTo(db.parroquias, {
    foreignKey: 'parr_id',
});
/* db.identidad.hasOne(db.datosafectado, {
    foreignKey: 'parr_id',
}); */
db.datosafectado.belongsTo(db.identidad, {
    foreignKey: 'iden_id'
});
db.datosafectado.belongsTo(db.genero, {
    foreignKey: 'gene_id'
});

db.datosafectado.belongsTo(db.discapacidad, {
    foreignKey: 'dis_id'
});



//====================================
// DENUNCIADO
//====================================

db.parroquias.hasOne(db.datosdenunciado, {
    foreignKey: 'parr_id'
});
db.datosdenunciado.belongsTo(db.parroquias, {
    foreignKey: 'parr_id'
});
db.datosdenunciado.belongsTo(db.identidad, {
    foreignKey: 'iden_id'
});
db.datosdenunciado.belongsTo(db.genero, {
    foreignKey: 'gene_id'
});
db.datosdenunciado.belongsTo(db.tipopersona, {
    foreignKey: 'tper_id'
});

//====================================
// DENUNCIANTE
//====================================
db.parroquias.hasOne(db.datosdenunciante, {
    foreignKey: 'parr_id',
});
db.datosdenunciante.belongsTo(db.parroquias, {
    foreignKey: 'parr_id',
});
db.datosdenunciante.belongsTo(db.identidad, {
    foreignKey: 'iden_id'
});
db.datosdenunciante.belongsTo(db.genero, {
    foreignKey: 'gene_id'
});
db.datosdenunciante.belongsTo(db.tipopersona, {
    foreignKey: 'tper_id'
});



module.exports = db;
module.exports.init = init;